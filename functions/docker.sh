#!/bin/bash

####################
#   DOCKER VARS    #
####################
# Make sure docker is installed so we dont error out trying to use it
if command -v docker >/dev/null; then
    docker_run(){
        $DOCKER_RUN \
            -v "${PWD}"/dist/:/etc/nginx/html/ \
            -v "${PWD}"/docker/nginx/conf.d/:/etc/nginx/conf.d/ \
            -v "${PWD}"/docker/nginx/snippets/:/etc/nginx/snippets/ \
            -v "${PWD}"/docker/nginx/ssl/:/etc/nginx/ssl/ \
            --network=vega \
            --ip="$1" \
            $2 $DOCKER_CONTAINER_NAME:$DOCKER_CONTAINER_TAG $3
    }

    docker_build(){
        $DOCKER_BUILD \
            -t $DOCKER_CONTAINER_NAME:$DOCKER_CONTAINER_TAG \
            -f "$DOCKER_DOCKERFILE" \
            --build-arg tz="$HOST_TIMEZONE" \
            --build-arg USERNAME="$HOST_USERNAME" \
            --build-arg UID="$HOST_UID" \
            --build-arg GID="$HOST_GID" .
    }

    docker_compose_run(){
        $DOCKER_COMPOSE_RUN
    }

    docker_compose_build(){
        $DOCKER_COMPOSE_BUILD \
            --build-arg tz="$HOST_TIMEZONE" \
            --build-arg USERNAME="$HOST_USERNAME" \
            --build-arg UID="$HOST_UID" \
            --build-arg GID="$HOST_GID"
    }
fi