#!/bin/bash

# Check if we have callback functions
if [ -f $PROJECT_DIR/functions/callbacks/*.sh ]; then
    . $PROJECT_DIR/functions/callbacks/*.sh
fi

# Usage:
# prompt $1: "Question" 
#        $2: "Default Value(Optional)"
#        $3: "Variable"
#        $4: "Callback function(Optional)"
#        $5: ".env File location"
#        $6: "Empty Allowed? (True/False)"
prompt(){
    # Keep retrying if an empty response is disallowed
    while [ -z $PROMPT ]; do
        # Check if we're adding a Default Value
        if [ ! -z $2 ]; then
            read -e -p "$1: " -i $2 PROMPT
        else
            read -p "$1: " PROMPT
        fi

        # Break if an empty response is allowed
        if [ $6 = "True" ]; then
            break
        elif [ -z $PROMPT ]; then
            echo "Field is required!"
        fi
    done

    # If callback function is specified pass all arguments
    if [ ! -z $4 ]; then
        $4 "$@"
    fi

    # Output to .env file to be used in docker container
    # or Helix environment
    if [ ! -z $5 ]; then
        echo "$3='$PROMPT'" >> $5
    fi

    # Flush PROMPT var so we dont set all variables to first
    unset PROMPT
}