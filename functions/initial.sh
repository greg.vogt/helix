#!/bin/bash

####################
#   Installation   #
####################
# Source release file so we can use the vars
if [ -f /etc/lsb-release ]; then
    . /etc/lsb-release

    # Check if we have all the programs we need
    for p in docker docker-compose jq openssl; do
        if command -v $p >/dev/null; then
            echo "Found $p"
        else
            # Arch Linux and derivatives
            if [ "$DISTRIB_ID" == "ManjaroLinux" ] || [ "$DISTRIB_ID" == "Arch" ]; then
                if command -v pacman >/dev/null; then
                    echo "======================================================"
                    echo "# Elevation required to install required packages    #"
                    echo "======================================================"
                    sudo pacman -Sy $p
                else
                    echo "======================================================"
                    echo "# Detected Arch derivative but couldn't find pacman! #"
                    echo "======================================================"
                    exit 1;    
                fi
            fi
        fi
    done
else
    echo "======================================================"
    echo "# Can't find /etc/lsb-release. Installer is unaware  #"
    echo "# of linux environment! Please install the following #"
    echo "# packages manually before proceeding:               #"
    echo "# docker docker-compose jq openssl                   #"
    echo "======================================================"
    exit 1;
fi

# So our docker container runs as non-root user
if [ ! -f $PWD/env/base.env ]; then
    mkdir -p $PWD/env
    touch $PWD/env/base.env
    echo "HOST_TIMEZONE='$(cat /etc/timezone)'"  >> $PWD/env/base.env
    echo "HOST_USERNAME='$(whoami)'" >> $PWD/env/base.env
    echo "HOST_UID='$(id -u)'" >> $PWD/env/base.env
    echo "HOST_GID='$(id -g)'" >> $PWD/env/base.env
fi

# Find all modules
declare -a MODULES

# https://stackoverflow.com/a/11584156
# Enable special handling to prevent expansion to a
# literal '/tmp/backup/*' when no matches are found. 
shopt -s nullglob

FOLDERS=($PROJECT_DIR/modules/*)
for folder in "${FOLDERS[@]}"; do
    [[ -d "$folder" ]] && MODULES+=( "$(basename $folder)" )
done

# Unset shell option after use, if desired. Nullglob
# is unset by default.
shopt -u nullglob