#!/bin/bash

# Make sure we exit if there's any errors; prevents
# mis-configured environment
set -e

PROGRAM_NAME="Helix v0.1"
PROGRAM_DESC="Modular wrapper for various docker and development functions"
PROGRAM_FILENAME="${0##*/}"

# So other files can reference from the base directory later on
PROJECT_DIR=$PWD

####################
#  INITIAL SETUP   #
####################
. $PROJECT_DIR/functions/initial.sh

####################
#  EXTERNAL VARS   #
####################
. $PROJECT_DIR/env/base.env

# Used alongside eval to run commands with direct variable exports
BASE_ENV_SOURCED="$(grep -hv '^#' $PROJECT_DIR/env/base.env | xargs)"

####################
#  EXTERNAL FUNCS  #
####################
. $PROJECT_DIR/functions/json.sh
. $PROJECT_DIR/functions/docker.sh
. $PROJECT_DIR/functions/prompt.sh

# Variables made available from initial.sh and sourced by base.env:
#   HOST_TIMEZONE
#   HOST_USERNAME
#   HOST_UID
#   HOST_GID

####################
#    HELP FUNC     #
####################
help(){
    echo "${PROGRAM_NAME}; $(fold -s <<< ${PROGRAM_DESC})"
    echo
    echo "Syntax: ${PROGRAM_FILENAME} <subscript> [-c|b|r|d|x|s] <arguments>"
    echo "options:"
    echo "  ${PROGRAM_FILENAME} -h      Print this help."
    echo
    echo " ============== "
    echo " # SUBSCRIPTS # "
    echo " ============== "
    echo "  ${PROGRAM_FILENAME} config -s      Start config process."
    echo "  ${PROGRAM_FILENAME} config -e      Edit existing config."
    echo "  ${PROGRAM_FILENAME} config -h      Print config subscripts help."
    echo
    echo "  ${PROGRAM_FILENAME} docker -c      Clean build directory"
    echo "  ${PROGRAM_FILENAME} docker -b      (Re-)Build container"
    echo "  ${PROGRAM_FILENAME} docker -r      Run container"
    echo "  ${PROGRAM_FILENAME} docker -d      Daemonize container (--restart=always)"
    echo "  ${PROGRAM_FILENAME} docker -x      Pass command line arguments to docker"
    echo "  ${PROGRAM_FILENAME} docker -f      CONTAINER ONLY: First run in container"
    echo "  ${PROGRAM_FILENAME} docker -s      CONTAINER ONLY: Start internal services"
    echo "  ${PROGRAM_FILENAME} docker -h      Print docker subscripts help."
    echo
}

####################
#    BASE FLAGS    #
####################
while getopts ':hc' option; do
    case "$option" in
        h )  
            help >&2
            exit
        ;;
        c )
            :
        ;;
        : )  
            printf "missing argument for -%s\n" "$OPTARG" >&2
            help >&2
            exit 1
        ;;
        \? ) 
            printf "illegal option: -%s\n" "$OPTARG" >&2
            help >&2
            exit 1
        ;;
    esac
done

####################
#    SUBSCRIPTS    #
####################
if [ -n "${1}" ]; then
    # Check if the module name provided matches one we have and
    # if the modules helix.json file is present
    if [[ "${MODULES[@]}" =~ "${1}" ]]; then
        if [ -f "${PROJECT_DIR}/modules/${1}/helix.json" ]; then
            MODULE=$1; shift
        else
            echo "Helix.json file not found in module ${1}"
            exit 1
        fi
    else
        echo "Module ${1} not found!"
        exit 1
    fi

    if [ -n "${1}" ]; then
        subcommand=${1}; shift # Pop start.sh from the stack
        
        case "$subcommand" in
            config )
                . $PROJECT_DIR/subscripts/config.sh $1
            ;;
            docker )
                . $PROJECT_DIR/subscripts/docker.sh $1
            ;;
            * )
                help >&2
                exit 1
            ;;
        esac
    else
        help >&2
        exit 1
    fi
else
    help >&2
    exit 1
fi

