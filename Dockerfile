FROM archlinux:latest AS base
MAINTAINER Greg Vogt <greg.vogt@vogson.io>

ARG USERNAME
ARG UID
ARG GID

# Make sure we're using the fastest mirrors before starting
RUN pacman --noconfirm -Sy pacman-contrib reflector && \
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup && \
    reflector --verbose --protocol https --sort rate --save /etc/pacman.d/mirrorlist

# Update system
RUN pacman --noconfirm -Syyu

RUN groupadd -g ${GID} ${USERNAME} && \
    useradd -m -u ${UID} -g ${GID} -G sudo ${USERNAME}  && \
    chown -R ${UID}:${GID} /home/${USERNAME}

# Set us to the host user
USER {USERNAME}

# Change to what ever script you need to run or
# use the Helix launcher
CMD [ "/Helix/start.sh", "-s" ]