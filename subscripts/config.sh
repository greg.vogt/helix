#!/bin/bash

####################
#    HELP FUNC     #
####################
help(){
    echo "${PROGRAM_NAME}; $(fold -s <<< ${PROGRAM_DESC})"
    echo
    echo "Syntax: ${PROGRAM_FILENAME} <subscript> [-c|b|r|d|x|s] <arguments>"
    echo "options:"
    echo "  ${PROGRAM_FILENAME} config -s      Start config process."
    echo "  ${PROGRAM_FILENAME} config -e      Edit existing config."
    echo "  ${PROGRAM_FILENAME} config -h      Print this help."
    echo
}

####################
#    BASE FLAGS    #
####################
if [ -n "${1}" ]; then
    while getopts ':seh' option; do
        case "$option" in
            s )
                prompt "What's the name of your project? [IE: Helix]" "" "CONFIG_NAME" "" "$PROJECT_DIR/env/base.env" "False"
                prompt "Docker container name [IE: helix]" "" "CONTAINER_NAME" "" "$PROJECT_DIR/env/base.env" "False"
                prompt "Docker container version [IE: latest]" "" "CONTAINER_VER" "" "$PROJECT_DIR/env/base.env" "False"
                prompt "Docker local subnet; without last octet [IE: 172.0.20]" "" "CONTAINER_SUBNET" "" "$PROJECT_DIR/env/base.env" "False"
            ;;
            h )  
                help >&2
                exit
            ;;
            : )  
                printf "missing argument for -%s\n" "$OPTARG" >&2
                help >&2
                exit 1
            ;;
            \? ) 
                printf "illegal option: -%s\n" "$OPTARG" >&2
                help >&2
                exit 1
            ;;
        esac
    done
else
    help >&2
    exit 1 
fi