#!/bin/bash

####################
#    HELP FUNC     #
####################
help(){
    echo "${PROGRAM_NAME}; $(fold -s <<< ${PROGRAM_DESC})"
    echo
    echo "Syntax: ${PROGRAM_FILENAME} <subscript> [-c|b|r|d|x|s] <arguments>"
    echo "options:"
    echo "  ${PROGRAM_FILENAME} docker -f      Sends first run command to modules helix.sh handler file"
    echo "  ${PROGRAM_FILENAME} docker -b      (Re-)Build container"
    echo "  ${PROGRAM_FILENAME} docker -r      Run container"
    echo "  ${PROGRAM_FILENAME} docker -d      Daemonize container (--restart=always)"
    echo "  ${PROGRAM_FILENAME} docker -x      Pass command line arguments to docker"
    echo "  ${PROGRAM_FILENAME} docker -f      CONTAINER ONLY: First run in container"
    echo "  ${PROGRAM_FILENAME} docker -s      CONTAINER ONLY: Start internal services"
    echo "  ${PROGRAM_FILENAME} docker -h      Print this help."
    echo
}

# Loads modules helix file
json "0" "$PROJECT_DIR/modules/${MODULE}/helix.json"

HELIX=$(json 0 "$PROJECT_DIR/modules/${MODULE}/helix.json")

DOCKER_CONTAINER_NAME="${CONTAINER_NAME}"
DOCKER_CONTAINER_TAG="${CONTAINER_VER}"

DOCKER_DOCKERFILE="${PWD}"/docker/Dockerfile
DOCKER_RUN="docker run"
DOCKER_BUILD="docker build"
DOCKER_NETWORKS_IN_USE="$(docker inspect --format='{{.Name}} {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -q) 2>/dev/null || true)"

DOCKER_COMPOSE_RUN="eval $HELIX $BASE_ENV_SOURCED docker-compose -f $PROJECT_DIR/modules/${MODULE}/docker-compose.yml up"
DOCKER_COMPOSE_BUILD="eval $HELIX $BASE_ENV_SOURCED docker-compose -f $PROJECT_DIR/modules/${MODULE}/docker-compose.yml build"
DOCKER_COMPOSE_ATTACH="eval $HELIX $BASE_ENV_SOURCED docker-compose -f $PROJECT_DIR/modules/${MODULE}/docker-compose.yml run nginx /bin/sh"
DOCKER_COMPOSE_DOWN="eval $HELIX $BASE_ENV_SOURCED docker-compose -f $PROJECT_DIR/modules/${MODULE}/docker-compose.yml down"

####################
#    BASE FLAGS    #
####################
if [ -n "${1}" ]; then
    while getopts ':hbradfd' option; do
        case "$option" in
            h )  
                help >&2
                exit
            ;;
            b )
                $DOCKER_COMPOSE_BUILD
            ;;
            r )
                $DOCKER_COMPOSE_RUN
            ;;
            a ) 
                $DOCKER_COMPOSE_ATTACH
            ;;
            d ) 
                $DOCKER_COMPOSE_DOWN
            ;;
            f )
                . $PROJECT_DIR/modules/${MODULE}/helix.sh first_run
            ;;
            # d )
            #     . $PROJECT_DIR/modules/${MODULE}/helix.sh devel
            # ;;
            : )  
                printf "missing argument for -%s\n" "$OPTARG" >&2
                help >&2
                exit 1
            ;;
            \? ) 
                printf "illegal option: -%s\n" "$OPTARG" >&2
                help >&2
                exit 1
            ;;
            * )
                help >&2
                exit 1
            ;;
        esac
    done
else
    help >&2
    exit 1 
fi