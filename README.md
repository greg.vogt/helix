### Helix: a multi-featured docker bootstraper

A set of scripts designed to simplify standardizing docker environments across multiple Linux distros. Mainly geared for use in large projects where preparing the environemnt can be cumbersome.

Through the use of JSON files Helix can prepare the target system with basic functionality, IE: installing specific docker version, creating network stack that docker will use, even imaging an instance running on one system and standing it back up on the target machine.

And if theres something Helix cannot manage, creating a config.sh or docker.sh file in the folder of the intended project will hand full control to the end-user.

However, this repo is not currently feature complete with the VEGA version as it wasn't initially made to be a stand alone project.